This directory holds the test server's key when generated using:

    openssl req -x509 -newkey rsa:4096 -keyout test/ssl/private/key.pem -out test/ssl/certs/cert.pem -days 365 -nodes
