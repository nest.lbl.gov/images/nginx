#
# Script to launch `nginx` and supporting SAML2 deamons
#

SHIBD_CONFIGURATION_FILE="/etc/shibboleth/shibboleth2.xml"

SHIBBOLETH2_XML=${SHIBBOLETH2_XML_FILE:=/etc/nginx/conf.d/shibboleth/shibboleth2.xml}
if [ -f "${SHIBBOLETH2_XML}" ] ; then
    if [ -L "${SHIBD_CONFIGURATION_FILE}" ] ; then
	rm -f ${SHIBD_CONFIGURATION_FILE}
    fi
    if [ -f "${SHIBD_CONFIGURATION_FILE}" ] ; then
	mv ${SHIBD_CONFIGURATION_FILE} ${SHIBD_CONFIGURATION_FILE}.$(date +%Y-%m-%d)
    fi
    ln -s ${SHIBBOLETH2_XML} ${SHIBD_CONFIGURATION_FILE}
    service shibd status > /dev/null 2> /dev/null
    if [ 0 != ${?} ] ; then
        service shibd start
    fi
else
    if [ "X" != "X${NGINX_USER}" -a "X" != "X${NGINX_GROUP}" ] ; then
        SHIBBOLETH_DIR=$(dirname ${SHIBBOLETH2_XML})
        # Ignore error, e.g. in case the mount point is read only.
	mkdir -p ${SHIBBOLETH_DIR}
        cp -n /opt/shibboleth-sp-fastcgi/nest-nginx/*.dist \
	   /opt/shibboleth-sp-fastcgi/nest-nginx/*.nest-nginx \
           /opt/shibboleth-sp-fastcgi/nest-nginx/*.xml ${SHIBBOLETH_DIR}/ 2> /dev/null
	chown ${NGINX_USER}:${NGINX_GROUP} ${SHIBBOLETH_DIR} ${SHIBBOLETH_DIR}/*.dist ${SHIBBOLETH_DIR}/*xml ${SHIBBOLETH_DIR}/*.nest-nginx
        if [ -d "/etc/nginx/ssl/certs" -a -d "/etc/nginx/ssl/private" ] ; then
            if [ ! -f "/etc/nginx/ssl/certs/sp-encrypt.crt" -a ! -f "/etc/nginx/ssl/private/sp-encrypt.key" ] ; then
                openssl req -x509 -newkey rsa:4096 -subj "/CN=$(hostname)" \
                    -out /etc/nginx/ssl/certs/sp-encrypt.crt \
                    -keyout /etc/nginx/ssl/private/sp-encrypt.key -days 3650 -nodes
                chown ${NGINX_USER}:${NGINX_GROUP} /etc/nginx/ssl/certs/sp-encrypt.crt /etc/nginx/ssl/private/sp-encrypt.key
                chmod 600 /etc/nginx/ssl/private/sp-encrypt.key
            fi
            if [ ! -f "/etc/nginx/ssl/certs/sp-signing.crt" -a ! -f "/etc/nginx/ssl/private/sp-signing.key" ] ; then
                openssl req -x509 -newkey rsa:4096 -subj "/CN=$(hostname)" \
                    -out /etc/nginx/ssl/certs/sp-signing.crt \
                    -keyout /etc/nginx/ssl/private/sp-signing.key -days 3650 -nodes
                chown ${NGINX_USER}:${NGINX_GROUP} /etc/nginx/ssl/certs/sp-signing.crt /etc/nginx/ssl/private/sp-signing.key
                chmod 600 /etc/nginx/ssl/private/sp-signing.key
            fi
        fi
    fi
fi

if [ -f /etc/supervisor/supervisord.conf ] ; then
    service supervisor status > /dev/null 2> /dev/null
    if [ 0 != ${?} ] ; then
        service supervisor start
    fi
fi

nginx -g 'daemon off;'
