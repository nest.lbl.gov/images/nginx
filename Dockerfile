#
# This Dockerfile extends the standard `nginx` image
#   by customizing its configuration, adding the ability to
#   use shibboleth authentication, and installing the CIlogin,
#   Cyber Infrastructure certificates.

#
# This first build downloads the `cilogin-basic.pem` used for
#   http client verification.
#

FROM alpine:3.21.2 as certificates

RUN apk add --no-cache wget \
              openssl

# The are RUN separately as none of these layers appear in the final image
RUN wget https://cilogon.org/cilogon-ca-certificates.tar.gz \
    && wget -O /etc/ssl/certs/cilogon-osg.pem https://osg-htc.org/security/cilogonosgca/cilogon-osg.pem
RUN tar zxf cilogon-ca-certificates.tar.gz \
    cilogon-ca/certificates/cilogon-basic.pem \
    cilogon-ca/certificates/cilogon-silver.pem \
    cilogon-ca/certificates/cilogon-openid.pem
RUN mv cilogon-ca/certificates/cilogon-basic.pem /etc/ssl/certs/cilogin-basic.pem \
    && openssl x509 -noout -hash -in /etc/ssl/certs/cilogin-basic.pem > /etc/ssl/certs/cilogin-basic.hash
RUN mv cilogon-ca/certificates/cilogon-silver.pem /etc/ssl/certs/cilogin-silver.pem \
    && openssl x509 -noout -hash -in /etc/ssl/certs/cilogin-silver.pem > /etc/ssl/certs/cilogin-silver.hash
RUN mv cilogon-ca/certificates/cilogon-openid.pem /etc/ssl/certs/cilogin-openid.pem \
    && openssl x509 -noout -hash -in /etc/ssl/certs/cilogin-openid.pem > /etc/ssl/certs/cilogin-openid.hash
RUN openssl x509 -noout -hash -in /etc/ssl/certs/cilogon-osg.pem > /etc/ssl/certs/cilogon-osg.hash

RUN wget https://cert.ca.ngs.ac.uk/escience-root.cer \
    && wget https://cert.ca.ngs.ac.uk/escience2b.cer
RUN openssl x509 -inform DER -outform PEM -text -in escience-root.cer -out /etc/ssl/certs/escience-root.pem \
    && openssl x509 -noout -hash -in /etc/ssl/certs/escience-root.pem > /etc/ssl/certs/escience-root.hash
RUN openssl x509 -inform DER -outform PEM -text -in escience2b.cer -out /etc/ssl/certs/escience2b.pem \
    && openssl x509 -noout -hash -in /etc/ssl/certs/escience2b.pem > /etc/ssl/certs/escience2b.hash
    
#
# This adds any new dynamic modules that may be required
# This rebuilds shibboleth to enable the FASTCGI interface
#

FROM nginx:1.26.2 AS builder

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y apt-utils \
    && apt-get install -y curl gnupg2 ca-certificates lsb-release debian-archive-keyring wget
RUN wget -O /tmp/nginx_signing.key https://nginx.org/keys/nginx_signing.key \
    && gpg --dearmor /tmp/nginx_signing.key > /usr/share/keyrings/nginx-archive-keyring.gpg \
    && rm -r /tmp/nginx_signing.key
#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  0E98404D386FA1D9 6ED0E7B82643E131 F8D2585B8783D481
RUN apt-get install -y apt-utils \
        wget \
        build-essential \
        libboost-all-dev \
        liblog4cpp5-dev \
        libxerces-c-dev \
        libxml-security-c-dev \
        pkg-config \
        libcurl4-openssl-dev  \
        libssl-dev \
        zlib1g-dev \
	apache2-dev \
        libfcgi-dev \
        libxmltooling-dev \
        libsaml-dev

ENV SHIBBOLETH_SP_VERSION=3.5.0

# https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2067398689/LinuxBuild
RUN wget https://shibboleth.net/downloads/service-provider/${SHIBBOLETH_SP_VERSION}/shibboleth-sp-${SHIBBOLETH_SP_VERSION}.tar.gz \
    && tar zxvf shibboleth-sp-${SHIBBOLETH_SP_VERSION}.tar.gz  \
    && cd shibboleth-sp-${SHIBBOLETH_SP_VERSION} \
    && export PKG_CONFIG_PATH=/opt/shibboleth-sp/lib/pkgconfig \
    && ./configure --prefix=/opt/shibboleth-sp-fastcgi --with-fastcgi \
    && make \
    && make install \
    && cd ..

RUN apt-get install -y git libpcre3-dev \
    && wget "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O nginx-${NGINX_VERSION}.tar.gz \
    && tar zxvf nginx-${NGINX_VERSION}.tar.gz \
    && git clone https://github.com/nginx-shib/nginx-http-shibboleth.git \
    && git clone https://github.com/openresty/headers-more-nginx-module.git \
    && cd nginx-${NGINX_VERSION} \
    && CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
    && ./configure "${CONFARGS}" --with-compat \
        --add-dynamic-module=../nginx-http-shibboleth \
        --add-dynamic-module=../headers-more-nginx-module \
    && make modules


#
# This build creates the final image.
#

FROM nginx:1.26.2

LABEL maintainer="Simon Patton <sjpatton@lbl.gov, Keith Beattie <ksbeattie@lbl.gov>"

RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y apt-utils \
        shibboleth-sp-utils \
        supervisor \
	patch

# Install python3 as default and gixy to check configurations
RUN apt-get install -y python3 \
    && apt-get install -y python3-distutils \
    && apt-get install -y python3-pip \
    && update-alternatives --install /usr/local/bin/python python /usr/bin/python3 40 \
    && pip3 install gixy --break-system-packages

# Create exported VOLUME locations and make /var/cache/nginx/ writable by non-root users
RUN mkdir -p /etc/nginx/ssl \
    && chown nginx:nginx /etc/nginx/ssl \
    && chmod 600 /etc/nginx/ssl \
    && mkdir -p /usr/share/nginx/html \
    && chown nginx:nginx /usr/share/nginx/html \
    && mkdir -p /usr/share/nginx/secure \
    && chown nginx:nginx /usr/share/nginx/secure \
    && mkdir -p /var/cache/nginx/ \
    && chown nginx:nginx /var/cache/nginx/ \
    && chmod 777 /var/cache/nginx/ \
    && mkdir -p /var/log/supervisor \
    && chmod 777 /var/log/supervisor \
    && mkdir -p /var/run/shibboleth/ \
    && chown _shibd:_shibd /var/run/shibboleth/ \
    && mkdir -p /opt/shibboleth-sp/

# Add in file to run FASTCGI deamons for Shibboleth
COPY supervisor_shibboleth.conf /etc/supervisor/conf.d/shibboleth.conf

# Add in rebuilt shibboleth binaries and libraries
COPY --from=builder /opt/shibboleth-sp-fastcgi /opt/shibboleth-sp-fastcgi
COPY shibboleth /opt/shibboleth-sp-fastcgi/nest-nginx

RUN ldconfig \
    && mv /usr/sbin/shibd /usr/sbin/shibd.$(date +%Y-%m-%d) \
    && ln -s /opt/shibboleth-sp-fastcgi/sbin/shibd /usr/sbin/ \
    && mv /etc/shibboleth /etc/shibboleth.$(date +%Y-%m-%d) \
    && ln -s /opt/shibboleth-sp-fastcgi/etc/shibboleth /etc/ \
    && for f in $(ls -1 /etc/shibboleth/*.xml.dist) ; do cp $f /opt/shibboleth-sp-fastcgi/nest-nginx/ ; done \
    && cp /opt/shibboleth-sp-fastcgi/nest-nginx/shibboleth2.xml.dist /opt/shibboleth-sp-fastcgi/nest-nginx/shibboleth2.xml.nest-nginx \
    && patch /opt/shibboleth-sp-fastcgi/nest-nginx/shibboleth2.xml.nest-nginx /opt/shibboleth-sp-fastcgi/nest-nginx/patch_shibboleth2.xml \
    && rm -f /opt/shibboleth-sp-fastcgi/nest-nginx/patch_shibboleth2.xml


# Add dynamic modules created above
COPY --from=builder /nginx-${NGINX_VERSION}/objs/*.so /etc/nginx/modules/

# Run as port 8080 and 8443, which are available to non-root users and
# allows us to drop all remaining root capabilities from the container,
# which improves security.
EXPOSE 8080 8443

COPY --from=certificates /etc/ssl/certs/cilog?n-* /etc/ssl/certs/escience* /etc/ssl/certs/

RUN export hash=$(cat /etc/ssl/certs/cilogin-basic.hash) \
    && ln -s cilogin-basic.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/cilogin-basic.hash \
    && unset hash
RUN export hash=$(cat /etc/ssl/certs/cilogin-silver.hash) \
    && ln -s cilogin-silver.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/cilogin-silver.hash \
    && unset hash
RUN export hash=$(cat /etc/ssl/certs/cilogin-openid.hash) \
    && ln -s cilogin-openid.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/cilogin-openid.hash \
    && unset hash
RUN export hash=$(cat /etc/ssl/certs/cilogon-osg.hash) \
    && ln -s cilogon-osg.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/cilogon-osg.hash \
    && unset hash
RUN export hash=$(cat /etc/ssl/certs/escience-root.hash) \
    && ln -s escience-root.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/escience-root.hash \
    && unset hash
RUN export hash=$(cat /etc/ssl/certs/escience2b.hash) \
    && ln -s escience2b.pem /etc/ssl/certs/${hash}.0 \
    && rm /etc/ssl/certs/escience2b.hash \
    && unset hash

# Replaced default `nginx.conf` with SSL hardened version.
# Note: ssl-hardened.conf nneds to write PID file to a
#   location where regular users have write access,
#   e.g. /var/tmp/nginx.pid
COPY ssl-hardened.conf /etc/nginx/nginx.conf
COPY shib_clear_headers shib_fastcgi_params /etc/nginx/
COPY docker-entrypoint.sh /usr/local/bin/

# Add in a command to check health of URLs
COPY url_health /usr/local/bin/

VOLUME ["/etc/nginx/conf.d", \
        "/etc/nginx/ssl", \
        "/usr/share/nginx/html", \
        "/usr/share/nginx/secure" ]

CMD "docker-entrypoint.sh"
