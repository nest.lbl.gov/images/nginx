# Using SAML with nginx #

The [`lblnest/nginx`](https://hub.docker.com/repository/docker/lblnest/nginx) image already includes [SAML2](https://wiki.oasis-open.org/security/FrontPage) support in the form of the [`nginx-http-shibboleth`](https://github.com/nginx-shib/nginx-http-shibboleth) module.

This document is a guide on how to configure a deployment so that it can host a Service Provider(SP) instance. This document is **not** a guide to SAML2 and assumes enough basic knowledge to understand the terms used, such as "Service Provider", "Identity Provider", "Metadata", etc., or that the read will be able to derive their meaning from the context in which they appear.


## Preparing a Basic Service Provider Instance ##

Before enabling a Service Provider by deploying an appropriate `conf.d` file or files, it is preferable to start the `nginx` container with a vanilla configuration ([see the README.md](README.md), along with **Note 1** below) The reason being is that if the shibboleth files mentioned below do not exist, but their location is writable, starting the image will create and populate that area.

---
**Note 1**: As noted above, for the initial deployment the some mount points need to be writable, therefore the example `docker` command from the [README.md](README.md) should be rewritten, for the first deployment only, as:

    docker run --name nest-nginx -d \
        -e NGINX_USER=$(id -u) \
        -e NGINX_GROUP=$(id -g) \
        -v $(pwd)/conf.d:/etc/nginx/conf.d \
        -v $(pwd)/ssl:/etc/nginx/ssl \
        -v $(pwd)/html:/usr/share/nginx/html:ro \
        -v $(pwd)/secure:/usr/share/nginx/secure:ro \
        -p 8080:8080 \
        -p 8443:8443 \
        lblnest/nginx

---

Here, "vanilla" mean that the following file (or the one pointed to by the  `SHIBBOLETH2_XML` environmental variable) does not exist within the container (including any externally mounted volumes),

    /etc/nginx/conf.d/shibboleth/shibboleth2.xml

This is the main configuration file to handling that SAML2 protocol using Shibbolith.

When the above file does not exist, starting the image will create a suitable area (if privileges allow) for the Service Provider's configuration file. The default location is (within the container):

    /etc/nginx/conf.d/shibboleth/

This writes to the `/etc/nginx/conf.d` mount point which should be outside the container and thus the configuration to persist between container instances.

This area will then be populated by the following files:

*   `attribute-map.xml`
*   `attribute-policy.xml`
*   `partner-metadata.xml`
*   `request-map.xml`
*   `shibboleth2.xml.nest-nginx`
*   `attribute-map.xml.dist`
*   `attribute-policy.xml.dist`
*   `example-metadata.xml.dist`
*   `protocols.xml.dist`
*   `request-map.xml.dist`
*   `security-policy.xml.dist`
*   `shibboleth2.xml.dist`

The first half of this list, with the exception of the `shibboleth2.xml.nest-nginx` file, are basic instances of required files that need to be populated for the Service Provider to work. The second half are example files that come as part of the Shibboleth distribution.

Once these files exist the initial container can be deleted and a new one, with the standard privileges mentioned in the [README.md](README.md), can be deployed, i.e. with `/etc/nginx/conf.d` being read only.

The first step to using Shibboleth in the `nginx` container is to create the `shibboleth2.xml` file.


### The `shibboleth2.xml` file ###

The `shibboleth2.xml` file is the main  configuration files for the `shibd` deamon and must exist for the container to deploy that deamon. The easiest way to create one is by executing the following command in the shibboleh configuration directory.

    cp /etc/nginx/conf.d/shibboleth/shibboleth2.xml.nest-nginx \
        /etc/nginx/conf.d/shibboleth/shibboleth2.xml

Now, this file needs the following modifications before it is useful.

*   The `entityID` attribute of the `ApplicationDefaults` element must be set to the identity of the Service Provider. ([See here](https://wiki.shibboleth.net/confluence/display/CONCEPT/EntityNaming) for a discussion on this.)

    An typical entityID is structures like this `https://<server>:8443/shibboleth`.

*   The `entityID` attribute of the `SSO` element must be set to match the identity of the Identity provider (IdP).

All other customizations are done in the other `.xml` files in this area.


### The `partner-metadata.xml` file ###

The `partner-metadata.xml` file contains the [metadata](https://www.oasis-open.org/committees/download.php/51890/SAML%20MD%20simplified%20overview.pdf) of the Identity Provider to be used by this Service Provider. (It is beyond the scope of this document to cover how to use more than one IdP in conjunction with a Discover Service.) This is normally provided by requesting its administrator to send you a copy or accessing a URL on the IdP.


### The `request-map.xml` file ###

The `request-map.xml` determines what parts of the web site are accessible by whom. The "empty" version of this simply requires a valid user in order to access any part of the site. The one modification you will need to do is change the `name` attribute of the `Host` element to be the name of you server.

Further discussions on how to use this file will occur later after the basic Service Provider is running.


### The `attribute-map` file ###

The `attribute-map` determines how the Service Provider turns SAML content into "attributes", the internal/neutral representation of information stored within user sessions.

To begin with the "empty" version is fine as no attributes are initially used.


### The `attribute-policy.xml` file ###

The `attribute-policy.xml` file determines which, if any, attributes should be "filtered" in order to prevent applications protected by the Service Provider from seeing data that violates whatever policies the filter implements.

Again, as no attributes are initially used the "empty" version is fine.


## Setting up `nginx` to use SAML2/Shibboleth ##

### The `ngnix` configuration ###

The get `nginx` to use Shibboleth, it needs to use the `fastcgi` interface to the `shibd` deamon. This requires changes to the vanilla configuration file to secure access, i.e. the [`<service>_8443.conf` file](https://gitlab.com/nest.lbl.gov/docker_images/-/tree/master/nginx#https-configuration-server_8443conf).
The following is an example of what that file should look like in order to use Shibboleth.

    server {
        listen      8443 ssl;
    
        server_name  <server>;
    
        ssl_certificate     /etc/nginx/ssl/certs/<server>.chained.crt;
        ssl_certificate_key /etc/nginx/ssl/private/<server>.key;
        
        location / {
            root   /usr/share/nginx/secure;
            index  index.html index.htm;
    
            shib_request /shibauthorizer;
            shib_request_use_headers on;
            include shib_clear_headers;
            more_clear_input_headers 'displayName' 'mail' 'persistent-id';

    #        include /etc/nginx/conf.d/services/<service>_8443.conf;
        }
    
        #FastCGI authorizer for Auth Request module
        location = /shibauthorizer {
            internal;
            include fastcgi_params;
            fastcgi_pass unix:/var/run/shibboleth/shibauthorizer.sock;
        }
    
        #FastCGI responder
        location /Shibboleth.sso {
            include fastcgi_params;
            fastcgi_pass unix:/var/run/shibboleth/shibresponder.sock;
        }
    }

As can be seen, two new locations have been defined. These are used to call out to two different processes, `shibauthorizer` and `shibresponder`, that communicate with the `shibd` deamon. There is also additional boilerplate statements that are used to require Shibboleth authentication to access any part of the server.
 

## Starting the Service Provider ##

Once the configuration files have been set up, the Shibboleth service can commence. The simplest approach is to redeploy the `lblnest/nginx` image. This allow it to set up and start the necessary services for the Shibboleth service.

However, before you can use this authentication method there is one step left to do, and that is to send your Service Provider's Metadata to the Identity Provider so that is can be installed there and the IdP will know who it is talking to. The metadata is available for the following URL and you can either send a copy to the IdP's administrator or send the URL for them to download it.

    https://<server>:8443/Shibboleth.sso/Metadata

Once that as been installed then the content of `https` server will now require Shibboleth authentication to be accessible.


## Preserving a Service Provider's Signature ##

The Service Provider's Metadata contains two essential items that must not change if you end up re-deploying it is a new location. These are:

*   It's `entityId`, as specified in the `shibboleth2.xml` file.

*   The x509 credentials used for encryption and signing messages.

The latter files are written into the /etc/nginx/ssl directory tree and so these will be preserved for any new deployment of the same Service.

