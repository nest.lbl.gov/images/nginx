# Using Shibboleth Service Provider 3 #

This document is a collection of notes dealing with how to use a Shibboleth Service Provider 3. Installation of such a provider is documented [elsewhere](saml2-deployment.md).


## Logout ##

Unless explicitly provided on a web page there is no way to logout of an existing session. Therefore if a page is know to be part of a Shibboleth protected service, it is useful to put in a logout link . The link should point to the following URL, where the URL after the `return=` part of the query string points to the page that should be displayed after logging out has been  successful.

    https://<server>/Shibboleth.sso/Logout?return=<protocol>://<server2>/path

In order for logged out URL to be the destination  of a redirect, it is necessary to modify the `shibbolth2.xml` file and change or add to the `<Session>` elements attributes so they include the following.


    redirectLimit="exact+whitelist" redirectWhitelist="<protocol>://<server2>/"

(**Note:** if a non-standard port is used on <server2>, then that needs to be included in the `redirectWhitelist` attribute.)


### "Security of LogoutResponse not established" ###

The error message "Security of LogoutResponse not established" can be generated at Logout when the IdP has not been configured to SAML standard, the LogoutResponse should be signed. In order to mitigate this error, if the IdP can not be changed, two changes need to be made. First in the `shibboleth2.xml` file the following attribute needs ot be added to the `<Logout>`.

     conf:policyId="unsigned-slo"

Second the following needs to be added to the `security-policy.xml`.

        <!-- Turns off the requirement of having signed LogoutResponses -->
        <Policy id="unsigned-slo">
            <PolicyRule type="NullSecurity"/>
        </Policy>

Now the Logout should be able to proceed as normal.

