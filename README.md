The `lblnest/nginx` image is a extension of the `nginx` image. The added features include:

*   A hardened SSL configuration that currently has a A+ rating from [Qualys SSL Labs](https://www.ssllabs.com/ssltest/) server test.

*   `VOLUME` mount points:

    +   `/etc/nginx/ssl`: For SSL credentials;

    +   `/etc/nginx/conf.d`: For customizing the local configurations;

    +   `/usr/share/nginx/html`: For unencrypted web content;

    +   `/usr/share/nginx/secure`: For unencrypted (both resticited and unrestricted)  web content.
    
    (*Note:* The last three mount points are not required, but advised, as they set the conventions that are used in the sections below on how to set up a server.)

*   Additional dynamic `nginx` modules. Presently the following are included
    +   nginx-http-shibboleth
    +   headers-more-nginx-module

*   Any required executables to support the above modules.

*   Cyber Infrastructure (CILogin) certificates.

*   Open Science Grid certificates.


# Persistent Volumes

The following moun points should map on to persistent volumes. For the example deployment given at the end of this document the following commands can create appropriate directories that can be contain the necessaary files, containing the example content shown below.

    export WEB_HOME=${HOME}/web
    mkdir -p ${WEB_HOME}/conf.d/services ${WEB_HOME}/ssl ${WEB_HOME}/html ${WEB_HOME}/secure


## The `nginx` configuration files (`/etc/nginx/conf.d`)

This is used as a mount point for the local configurations. The typical contents of this directory are two files, one for each port on a server, where the server name should replace the `<server>` placeholder. Also shown below is how to proxy a web service, <service>, that is running in a parallel container whose DNS name is `web-services`.

### HTTP configuration (`<server>_8080.conf`) ###

For the `<server>` deployment the `<server>_8080.conf` file configures the public, unsecured, port. Here is an example of its content.

    cat > ${WEB_HOME}/conf.d/localhost_8080.conf << EOF
    server {
        listen 8080;
    
        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
    
    #        include /etc/nginx/conf.d/services/<service>_8080.conf;
        }
    }
    EOF

The `root` statement points to the `VOLUME` mount point declared by the image for public, unencrypted, web content. The `include` statement can be uncommented in the case there are individual service configurations in the `services` sub-directory. (This is a technique to avoid the main configuration file from become too large.)

Here is what its content of that service's `conf` file should look like.

    export EXAMPLE_SERVICE=example
    cat > ${WEB_HOME}/conf.d/services/${EXAMPLE_SERVICE}_8080.conf << EOF
        location /${EXAMPLE_SERVICE}/ {
            return 301 https://\$host\$request_uri;
        }

        location /${EXAMPLE_SERVICE}/html {
            proxy_pass       http://web-services:8080/${EXAMPLE_SERVICE}/html
            proxy_set_header Host \$http_host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Host \$server_name;
            proxy_set_header X-Real-IP \$remote_addr;
        }
    EOF

In this case unencrypted requests for insecure content (`/<service>/html`) are passed onto the service deployment directly and all other service requests are redirected to the `https` connection.


### HTTPS configuration (`<server>_8443.conf`)

For the `<server>` deployment the `<server>_8443.conf` file configures the secured port. Here is its content.

    export SSL_SERVER_NAME=example.com
    cat > ${WEB_HOME}/conf.d/${SSL_SERVER_NAME}_8443.conf << EOF
    server {
        listen      8443 ssl;

        server_name  ${SSL_SERVER_NAME};

        ssl_certificate     /etc/nginx/ssl/certs/${SSL_SERVER_NAME}.chained.crt;
        ssl_certificate_key /etc/nginx/ssl/private/${SSL_SERVER_NAME}.key;
    
        location / {
            root   /usr/share/nginx/secure;
            index  index.html index.htm;

    #        include /etc/nginx/conf.d/services/<service>_8443.conf;
        }
    }
    EOF

The `ssl` statements is discussed below in the [_Encryption Credentials_](#the-encryption-credentials-etcnginxssl) section.

The `root` statement points to the `VOLUME` mount point declared by the image for encrypted web content and, again, the `include` statement can be uncommented for support a web service. Moreover in the exampe below the secure content has been split into two parts, one that requires no authentication, i.e. is public, and one that requires a basic HTML authentication.

    export EXAMPLE_SERVICE=example
    export SSL_SERVER_NAME=example.com
    cat > ${WEB_HOME}/conf.d/services/${EXAMPLE_SERVICE}_8443.conf << EOF
    location /${SSL_SERVER_NAME}/secure/unrestricted {
        proxy_pass       http://web-services:8080/${EXAMPLE_SERVICE}/local/report;
        proxy_set_header Host \$http_host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host \$server_name;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header X-Real-IP \$remote_addr;

        sub_filter_types '*';
        sub_filter       'http://\$server_name' 'https://\$server_name';
        sub_filter_once  off;
    }

    location /${SSL_SERVER_NAME}/secure/restricted {
        auth_basic "${SSL_SERVER_NAME} Authorization";

        proxy_pass       http://web-services:8080/${EXAMPLE_SERVICE}/local/command;
        proxy_set_header Host \$http_host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host \$server_name;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header X-Real-IP \$remote_addr;

        sub_filter_types '*';
        sub_filter       'http://\$server_name' 'https://\$server_name'
        sub_filter_once  off;
    }
    EOF

It can be seen in this file that the `<service>` URLs that require authorization (`/<service>/secure/restricted`) are protected by an suitable authorization statement (this example uses HTTP Basic Authentication for clarity, under real deployments a different authentication mechanism will normally be used). All other URLs are passed onto the service deployment directly. 

It should be noted here that in both cases the response returned from the `<service>` has its content edited to replace the `https` channel into any returned URL that was directed at this server under the assumption that this is being used for a RESTful interface.

It should also be noted that all requests are passed on to the service deployment in unencrypted `http`.


## The Encryption Credentials (`/etc/nginx/ssl`) ##

The `ssl` statements point to the files mounted on the `/etc/nginx/ssl` mount point as discussed above. The `<server>.chained.crt` file contains the chained certificates list necessary for browsers to verify the server's certificate, while the `<server>.key` contains the servers key.

It is important to restrict access to the Key files, so the following commands are a template for creating an appropriate directory tree.

    mkdir ssl
    cd ssl
    mkdir certs private
    chmod go-rwx private
    # Add the key files to the "private" directory and then
    chmod  go-rwx private/*key
    chmod u-x private/*key*
    cd ..

The creation of these files is beyond the scope of this document.


## Static Web Content (the `html` and `secure` directories) ##

The above example proxies all requests to a service to the appropriate URL. However request that are not for the service are handled by the static web pages. As set out in the above files, responses to `http` requests come for the `/usr/share/nginx/html` mount point, while responses to `https` requests come for the `/usr/share/nginx/secure` one.


## Unencrypted Web Content (`/usr/share/nginx/html`)

The following command sets up simple `index.html` files for the `http` area. (The SERVER wordis a place holder and should be replaced by the name of the server in question - just to make sure you are talking to the correct `nginx` instance!)

    cat > ${WEB_HOME}/secure/index.html << EOF
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>Index to the unencrypted SERVER site</title>
      </head>
      <body>
        <p>This is the unsecure test site.</p>
      </body>
    </html>
    EOF


## Encrypted Web Content (`/usr/share/nginx/secure`)

Similarly the next set of command do the same for the `https` area. (Again, the SERVER word is a place holder and should be replaced by the name of the server in question.)

    export SSL_SERVER_NAME=example.com
    cat > ${WEB_HOME}/secure/index.html << EOF
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>Index to the secure site</title>
      </head>
      <body>
        <p>This is the encrypted site for ${SSL_SERVER_NAME}.</p>
      </body>
    </html>
    EOF


## Deployment ##

The are many ways to deploy the `lblnest/nginx` image, for example straight from docker or using one of the many service orchestration systems. The details of all of these is beyond the scope of this document, but and example `docker` dommand is given below in order to easily test a set up of files. 

    docker run --name nest-nginx -d \
        -v $(pwd)/conf.d:/etc/nginx/conf.d:ro \
        -v $(pwd)/ssl:/etc/nginx/ssl:ro \
        -v $(pwd)/html:/usr/share/nginx/html:ro \
        -v $(pwd)/secure:/usr/share/nginx/secure:ro \
        -p 8080:8080 \
        -p 8443:8443 \
        lblnest/nginx

